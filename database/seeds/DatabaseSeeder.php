<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()

    {

       factory(App\cat::class , 50)->create();
        DB::table('roles')->insert(['name' => 'Administrator']);
        DB::table('roles')->insert(['name' => 'Admin']);
        DB::table('roles')->insert(['name' => 'User']);
        DB::table('users')->insert([
            'name' => 'Administrator','email' => 'admin@mail.com','role_id' => '1',
            'password' => Hash::make('admin')]);

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cat extends Model
{
    protected $fillable = ['name'];

    public function ticket()
    {
        return $this->hasMany('App\tickets');
    }
}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function role(){
        return $this->belongsTo('App\Role');
    }
    public function isAdministrator()
    {
        return $this->role->name == 'Administrator';
    }
    public function isAdmin()
    {
        return $this->role->name == 'Admin';
    }
    public function isUser()
    {
        return $this->role->name == 'User';
    }

    public function ticket()
    {
        return $this->hasMany('App\tickets');
    }

    protected $fillable = [
        'name', 'email', 'password', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

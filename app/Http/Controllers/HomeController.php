<?php

namespace App\Http\Controllers;

use http\Client\Curl\User;
use Illuminate\Http\Request;
use App\tickets;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();
        if($user->role_id == 1 || $user->role_id == 2){
            $tickets =  tickets::with('user')->paginate(10);
        } else{
            $tickets =  tickets::where('user_id', $user->id)->paginate(10);
        }
        return view('home' , compact('tickets'));    }


}

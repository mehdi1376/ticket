<?php

namespace App\Http\Controllers;

use App\Http\Requests\TicketUpdateRequest;
use App\tickets;
use App\cat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;

class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        if($user->role_id == 1 || $user->role_id == 2)
        {
            $tickets = tickets::with('user')->paginate(10);
        } else{
            $tickets = tickets::where('user_id', $user->id)->with('user')->paginate(10);
        }

        return view('tickets.index', compact('tickets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = DB::table('cats')->paginate(10);
        return view('tickets.create', compact('cats'));
    }

    public function newcat()
    {
        return view('tickets.createcat');
    }

    public function changep()
    {
        $roles = Role::get();
        $users = User::paginate(10);
        return view('tickets.changep', compact('users', 'roles'));
    }

    public function changepstore(Request $request, $user)
    {
        if ($user == 1) {
            return redirect()->route('tickets.changep')
                ->withSuccess('شما نمی توانید نوع کاربری را تغییر دهید');
        }
        $user = User::find($user);
        $user->role_id = request('role');
        $user->save();

        return redirect()->route('tickets.changep')->withSuccess('نوع کاربری با موفقیت تغییر کرد');
    }

    public function cats()
    {
        $cats = DB::table('cats')->paginate(10);
        return view('tickets.cats', compact('cats'));
    }

    public function newcatstore(Request $request)
    {
        cat::create([
            'name' => request('name'),
        ]);

        return redirect()->route('tickets.cats');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketUpdateRequest $request)
    {
        tickets::create([
            'summary' => request('summary'),
            'description' => request('description'),
            'user_id' => request('user_id'),
            'cat_id' => request('cat_id'),
            'status' => request('status'),
        ]);
        // dd($request);
        return redirect()->route('tickets.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\tickets  $tickets
     * @return \Illuminate\Http\Response
     */
    public function show($tickets)
    {
        $user = auth()->user();

        if ($user->role_id == 1 || $user->role_id == 2) {
            $tickets = tickets::find($tickets);
        } else{
            $tickets = tickets::where('user_id', $user->id)->where('id', $tickets)->firstOrFail();
        }
        return view('tickets.show', compact('tickets'));
    }

    public function addComment(Request $request, $ticket)
    {
        $ticket = tickets::find($ticket);

        $ticket->comments()->create([
            'body' => request('body'),
            'user_id' => auth()->id()
        ]);

        return redirect()->route('tickets.show', $ticket)->withSuccess('Comment has been added');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\tickets  $tickets
     * @return \Illuminate\Http\Response
     */
    public function edit(tickets $tickets)
    {
        //
    }

    /*------------------*/

    public function delete($tickets)
    {
        $tickets = tickets::find($tickets);
//        dd($tickets);
        return view('tickets.delete', compact('tickets'));
    }

    public function deletecat($cats)
    {
        $cats = cat::find($cats);
//        dd($cats);
        return view('tickets.deletecat', compact('cats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\tickets  $tickets
     * @return \Illuminate\Http\Response
     */
    public function update(TicketUpdateRequest $request, $tickets)
    {
        $user = auth()->user();

        if ($user->role_id == 1 || $user->role_id == 2) {
            $tickets1 = tickets::find($tickets);
        } else{
            $tickets1 = tickets::where('user_id', $user->id)->where('id', $tickets)->firstOrFail();
        }
        // dd($request);
        $tickets1->summary = request('summary');
        $tickets1->description = $request->input('description');
        $tickets1->status = request('status');
        $tickets1->save();

        return redirect()->route('tickets.index')->withSuccess('تیکت با موفقیت بروزرسانی شد.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\tickets  $tickets
     * @return \Illuminate\Http\Response
     */
    public function destroy($tickets)
    {
        tickets::find($tickets)->delete();
        return redirect()->route('tickets.index')->withSuccess('Ticket has Deleted');
    }

    public function destroycat($cats)
    {
        cat::find($cats)->delete();
        return redirect()->route('tickets.cats')->withSuccess('Category has Deleted');
    }
}

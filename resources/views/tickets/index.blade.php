@extends('layouts.main')




@section('content')
    @if(Auth::check())
    @endif
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

        @include('layouts.partials._alerts')

        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">سیستم پشتیبانی</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                {{--<div class="btn-group mr-2">--}}
                    {{--<button class="btn btn-sm btn-outline-secondary">Share</button>--}}
                    {{--<button class="btn btn-sm btn-outline-secondary">Export</button>--}}
                {{--</div>--}}
                <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                    <span data-feather="calendar"></span>
                    همین هفته
                </button>
            </div>
        </div>
        <a class="btn btn-primary" href="/tickets/create" >ایجاد تیکت جدید</a>
        @if (Auth::user()->isAdministrator())
        <a class="btn btn-primary" href="/create_cat" >ایجاد دسته بندی جدید</a>
            <a class="btn btn-primary" href="/Change_Permissions" >مدیریت کاربران</a>
        @endif
        {{--<canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>--}}
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>

                    <th>درخواست</th>
                    <th>کاربر</th>
                    <th>خلاصه درخواست</th>
                    <th>توضیحات تکمیلی</th>
                    <th>وضعیت</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tickets as $ticket)
                <tr>
                    <td>{{$ticket->id}}</td>
                    <td>{{$ticket->user->name}}</td>
                    <td>{{$ticket->summary}}</td>
                    <td>{{$ticket->description}}</td>
                    <td>{{$ticket->status}}</td>
                    <td><a class="btn btn-primary" href="/tickets/{{$ticket->id}}" >به روز رسانی</a></td>
                    <td><a class="btn btn-danger" href="/tickets/delete/{{$ticket->id}}" >حذف</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>

                {{$tickets->links()}}
        </div>
    </main>

@endsection

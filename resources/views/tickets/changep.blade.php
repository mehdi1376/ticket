@extends('layouts.main')




@section('content')
    @if(Auth::check())
    @endif
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

        @include('layouts.partials._alerts')

        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">سیستم پشتیبانی</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                {{--<div class="btn-group mr-2">--}}
                {{--<button class="btn btn-sm btn-outline-secondary">Share</button>--}}
                {{--<button class="btn btn-sm btn-outline-secondary">Export</button>--}}
                {{--</div>--}}
                <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                    <span data-feather="calendar"></span>
                    همین هفته
                </button>
            </div>
        </div>
        {{--<canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>--}}
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>

                    <th>ردیف</th>
                    <th>شناسه</th>
                    <th>نام</th>
                    <th>ایمیل </th>
                    <th>نوع کاربری</th>
                    <th> تغییر کاربری</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$user->id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->role->name}}</td>
                        <td>
                            <form action="/Change_Permissions/{{$user->id}}" method="POST">
                                @csrf
                                <div class="input-group">
                                    <select name="role" id="role" class="form-control">
                                        @foreach($roles as $role)
                                            <option value="{{ $role->id}}"
                                            @if($user->role_id == $role->id)
                                            selected
                                            @endif
                                            >{{ $role->name}}</option>
                                        @endforeach
                                    </select>
                                    <button type="submit" class="btn btn-sm btn-primary">به روز رسانی</button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{$users->links()}}
        </div>
    </main>

@endsection

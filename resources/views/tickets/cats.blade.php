@extends('layouts.main')




@section('content')

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">

        @include('layouts.partials._alerts')

        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">Ticketing System</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                {{--<div class="btn-group mr-2">--}}
                    {{--<button class="btn btn-sm btn-outline-secondary">Share</button>--}}
                    {{--<button class="btn btn-sm btn-outline-secondary">Export</button>--}}
                {{--</div>--}}
                <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                    <span data-feather="calendar"></span>
                    همین هفته
                </button>
            </div>
        </div>
        <a class="btn btn-primary" href="/tickets/create" >ایجاد درخواست جدید</a>
        <a class="btn btn-primary" href="/create_cat" >ایجاد دسته بندی جدید</a>
        {{--<canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>--}}
        <div class="table-responsive">
            <table class="table table-striped table-sm">
                <thead>
                <tr>
                    <th>درخواست</th>
                    <th>نام</th>
                    <th>عملیات</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cats as $cat)
                <tr>
                    <td>{{$cat->id}}</td>
                    <td>{{$cat->name}}</td>
                    <td><a class="btn btn-danger" href="/cats/delete/{{$cat->id}}" >حذف</a></td>
                </tr>
                @endforeach
                </tbody>
            </table>

                {{$cats->links()}}
        </div>
    </main>

@endsection

@extends('layouts.main')


@section('content')

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h1 class="h2">سیستم پشتیبانی</h1>
            <div class="btn-toolbar mb-2 mb-md-0">
                {{--<div class="btn-group mr-2">--}}
                    {{--<button class="btn btn-sm btn-outline-secondary">Share</button>--}}
                    {{--<button class="btn btn-sm btn-outline-secondary">Export</button>--}}
                {{--</div>--}}
                <button class="btn btn-sm btn-outline-secondary dropdown-toggle">
                    <span data-feather="calendar"></span>
                    همین هفته
                </button>
            </div>
        </div>
        {{--<canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>--}}
        <form action="{{ route('tickets.newcatstore') }}" method="post">
            {{ csrf_field()}}
            <div class="form-group">
                <label for="name">نام</label>
                <input type="text" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" id="name" name="name" placeholder="نام دسته بندی خود را وارد کنید">
                @if( $errors->has('name'))
                    <small id="name" name="name" class="form-text text-muted">{{$errors->first('name')}}</small>
                @endif
            </div>
            <button type="submit" class="btn btn-primary">ایجاد</button>
            <a href="{{route('tickets.cats')}}" class="btn btn-default">برگشت</a>
        </form>
    </main>

@endsection

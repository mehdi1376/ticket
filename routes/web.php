<?php
use App\tickets;
/*
Route::get('/p', function () {
    $u = App\User::find(1);
    $u->role_id = 1;
    $u->save();
    // tickets::create([
    //     'summary' => 'TEST',
    //     'description' => 'TEST',
    //     'user_id' => 2,
    //     'cat_id' => 1,
    //     'status' => 1,
    // ]);
});
*/
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});
Route::middleware(['auth'])->group(function () {
    Route::get('/tickets', 'TicketsController@index')->name('tickets.index');

    Route::get('/Change_Permissions', 'TicketsController@changep')->name('tickets.changep')->middleware(['isAdministrator']);

    Route::post('/Change_Permissions/{user}', 'TicketsController@changepstore')->name('tickets.changepstore')->middleware(['isAdministrator']);

    Route::get('/create_cat', 'TicketsController@newcat')->name('tickets.newcat')->middleware(['isAdministrator']);

    Route::get('/cats', 'TicketsController@cats')->name('tickets.cats')->middleware(['isAdministrator']);

    Route::post('/create_cat', 'TicketsController@newcatstore')->name('tickets.newcatstore')->middleware(['isAdministrator']);

    Route::get('/tickets/create', 'TicketsController@create')->name('tickets.create');

    Route::post('/tickets/create', 'TicketsController@store')->name('tickets.store');

    Route::post('/tickets/{ticket}', 'TicketsController@update')->name('tickets.update');

    Route::get('/tickets/delete/{ticket}', 'TicketsController@delete')->name('tickets.delete');

    Route::post('/tickets/delete/{ticket}', 'TicketsController@destroy')->name('tickets.destroy');

    Route::get('/cats/delete/{ticket}', 'TicketsController@deletecat')->name('tickets.deletecat');

    Route::post('/cats/delete/{ticket}', 'TicketsController@destroycat')->name('tickets.destroycat');

    Route::get('/tickets/{ticket}', 'TicketsController@show')->name('tickets.show');

    Route::post('/tickets/{ticket}/comments', 'TicketsController@addComment')->name('tickets.addComment');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
